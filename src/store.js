import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    files: [],
    dragOver: false
  },
  getters: {
    totalFiles (state) {
      return state.files.length
    },
    files (state) {
      return state.files
    }
  },
  mutations: {
    addFile (state, files) {
      state.files.unshift(files)
    },
    removeFile (state, path) {
      // console.log(path)
      state.files.splice(path.path, 1)
    },
    setDragOver (state, dragOver) {
      state.dragOver = dragOver
    }
  },
  actions: {
    addFile ({ commit }, path) {
      commit('addFile', path)
    }
  }
})
