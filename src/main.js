import Vue from 'vue'
import App from './App.vue'
import store from './store'

import 'vue-awesome/icons'
import Icon from 'vue-awesome/components/Icon'
import 'file-icon-vectors/dist/file-icon-vivid.min.css'

import Buefy from 'buefy'
import 'buefy/dist/buefy.css'

Vue.use(Buefy)

Vue.config.productionTip = false
Vue.component('v-icon', Icon)

new Vue({
  store,
  render: h => h(App)
}).$mount('#app')
