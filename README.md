# ![](build/icon_48x48.png) Clouseau

![](build/clouseau_screenshot.png)

Clouseau is a small GUI for the `ffprobe ` CLI-tool. It is a requrement that `ffprobe` is available on your `$PATH`.

It has only been tested on OS X.



## Development Setup
```
yarn install
```

##### Compiles and hot-reloads for development
```
yarn electron:serve
```

##### Compiles and minifies for production
```
yarn electron:build
```

##### Run your tests
```
yarn run test
```

##### Lints and fixes files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
