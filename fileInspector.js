const path = require('path')
const fs = require('fs')
const mime = require('mime')
const ffprobe = require('./ffprobe.js')

function sendFileToStore (win, file) {
  win.webContents.send('selected-files', file)
}

function processSelectedFiles (win, filePaths) {
  for (const filePath of filePaths) {
    let fileObj = path.parse(filePath)
    fileObj.path = filePath

    // if its a file
    if (fs.lstatSync(fileObj.path).isFile()) {
      fileObj.mimeType = mime.getType(fileObj.ext)
      const probeMimeTypes = ['video', 'audio', 'image']
      const mimeCategory = fileObj.mimeType.split('/')[0]
      if (probeMimeTypes.indexOf(mimeCategory) !== -1) {
        // console.log('it is a media file')
        ffprobe(fileObj.path, { path: '/usr/local/bin/ffprobe' })
          .then(function (stdout) {
            fileObj.metadata = stdout
            sendFileToStore(win, fileObj)
            // console.log(stdout)
          })
          .catch(function (err) {
            console.error(err)
          })
      } else {
        sendFileToStore(win, fileObj)
      }
    } else if (fs.lstatSync(fileObj.path).isDirectory()) {
      fileObj.isDir = true
      sendFileToStore(win, fileObj)
    }
  }
}

module.exports = {
  sendFileToStore,
  processSelectedFiles
}
